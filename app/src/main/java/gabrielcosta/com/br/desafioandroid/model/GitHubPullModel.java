package gabrielcosta.com.br.desafioandroid.model;

/**
 * Created by gabrielcosta on 16/07/16.
 */
public interface GitHubPullModel {

    void listPullRequest(String creator, String repository, int page);

}
