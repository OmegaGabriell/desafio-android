package gabrielcosta.com.br.desafioandroid.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import gabrielcosta.com.br.desafioandroid.R;
import gabrielcosta.com.br.desafioandroid.delegate.ViewHolderClickDelegate;
import gabrielcosta.com.br.desafioandroid.entity.Item;

/**
 * Created by gabrielcosta on 14/07/16.
 */
public class RepositoryListAdapter extends RecyclerView.Adapter<RepositoryHolder> {

    private List<Item> repositoryList;
    private Context context;
    private ViewHolderClickDelegate delegate;

    public RepositoryListAdapter(List<Item> list, Context context, ViewHolderClickDelegate delegate) {
        this.repositoryList = list;
        this.context = context;
        this.delegate = delegate;
    }

    @Override
    public RepositoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repository_list_item, parent, false);

        return new RepositoryHolder(view, this.delegate);
    }

    public void incrementAdapter(List<Item> items) {
        this.repositoryList.addAll(items);
        this.notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RepositoryHolder holder, int position) {
        Item repository = repositoryList.get(position);
        holder.repositoryName.setText(repository.getName());
        holder.repositoryDesc.setText(repository.getDescription());
        holder.repositoryForks.setText(String.valueOf(repository.getForks()));
        holder.repositoryStarts.setText(String.valueOf(repository.getWatchers()));
        holder.repositoryUsername.setText(repository.getOwner().getLogin());
        holder.userLastName.setText(repository.getOwner().getFullUserName());
        Picasso.with(context)
                .load(repository.getOwner().getAvatarUrl())
                .placeholder(R.drawable.ic_action_account_circle)
                .into(holder.repositoryImage);
    }

    @Override
    public int getItemCount() {
        return repositoryList.size();
    }
}
