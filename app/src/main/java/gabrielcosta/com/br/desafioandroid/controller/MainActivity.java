package gabrielcosta.com.br.desafioandroid.controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import gabrielcosta.com.br.desafioandroid.delegate.ViewHolderClickDelegate;
import gabrielcosta.com.br.desafioandroid.list.EndlessRecyclerOnScrollListener;
import gabrielcosta.com.br.desafioandroid.R;
import gabrielcosta.com.br.desafioandroid.list.RepositoryListAdapter;
import gabrielcosta.com.br.desafioandroid.delegate.GitHubDelegate;
import gabrielcosta.com.br.desafioandroid.entity.Item;
import gabrielcosta.com.br.desafioandroid.model.GitHubModel;
import gabrielcosta.com.br.desafioandroid.model.GitHubModelImpl;

public class MainActivity extends AppCompatActivity implements GitHubDelegate<List<Item>>, ViewHolderClickDelegate {

    private RecyclerView recyclerView;
    private RepositoryListAdapter adapter;
    private GitHubModel model;
    ProgressDialog progress;
    private List<Item> itemList;
    private int page = 1;

    private static final String LANGUAGE = "language:java";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.recyclerView = (RecyclerView) findViewById(R.id.repository_list);

        this.progress = this.buildProgressDialog();
        this.progress.show();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        this.model = new GitHubModelImpl(this);
        this.itemList = new ArrayList<>();
        this.adapter = new RepositoryListAdapter(this.itemList, this.getBaseContext(), this);
        this.recyclerView.setAdapter(this.adapter);
        this.loadList();

        this.recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                MainActivity.this.progress.show();
                MainActivity.this.loadList();
            }
        });

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private ProgressDialog buildProgressDialog() {
        ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Carregando");
        progress.setMessage("Carregando lista de repositorios");
        progress.setCancelable(false);
        return progress;
    }

    private void loadList() {
        this.model.listRepositoriesByLanguage(LANGUAGE, this.page++);
    }

    @Override
    public void onSucess(List<Item> items) {
        this.adapter.incrementAdapter(items);

        progress.dismiss();
    }

    @Override
    public void onFail(String message) {

        progress.dismiss();
    }

    @Override
    public void onItemclick(String creator, String repository) {
        Intent intent = new Intent(this, PullRequestActivity.class);
        intent.putExtra(PullRequestActivity.EXTRA_CREATOR, creator);
        intent.putExtra(PullRequestActivity.EXTRA_REPOSITORY, repository);
        this.startActivity(intent);
    }
}
