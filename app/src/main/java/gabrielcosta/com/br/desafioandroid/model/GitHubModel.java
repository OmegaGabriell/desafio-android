package gabrielcosta.com.br.desafioandroid.model;

/**
 * Created by gabriel.costa on 15/07/2016.
 */
public interface GitHubModel {

    void listRepositoriesByLanguage(String language, int page);

}
