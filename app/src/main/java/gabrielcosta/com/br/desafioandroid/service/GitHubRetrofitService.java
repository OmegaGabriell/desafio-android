package gabrielcosta.com.br.desafioandroid.service;

import java.util.List;
import java.util.Map;

import gabrielcosta.com.br.desafioandroid.entity.Pullrequest;
import gabrielcosta.com.br.desafioandroid.entity.RepositoryResult;
import gabrielcosta.com.br.desafioandroid.entity.UserInfo;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by gabrielcosta on 14/07/16.
 */
public interface GitHubRetrofitService {

    @GET("/search/repositories")
    Call<RepositoryResult> listRepositories(@QueryMap Map<String, String> options);

    @GET("repos/{creator}/{repository}/pulls")
    Call<List<Pullrequest>> listPullRequest(@Path("creator") String creator, @Path("repository") String repository, @Query("page") int page);

    @GET("/users/{login}")
    Call<UserInfo> getUserInfo(@Path("login") String login);

}
