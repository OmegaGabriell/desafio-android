package gabrielcosta.com.br.desafioandroid.delegate;

/**
 * Created by gabrielcosta on 16/07/16.
 */
public interface PullListClickDelegate {

    void onItemClick(String url);

}
