package gabrielcosta.com.br.desafioandroid.controller;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import gabrielcosta.com.br.desafioandroid.R;
import gabrielcosta.com.br.desafioandroid.delegate.GitHubDelegate;
import gabrielcosta.com.br.desafioandroid.delegate.PullListClickDelegate;
import gabrielcosta.com.br.desafioandroid.entity.Pullrequest;
import gabrielcosta.com.br.desafioandroid.list.EndlessRecyclerOnScrollListener;
import gabrielcosta.com.br.desafioandroid.list.PullListAdapter;
import gabrielcosta.com.br.desafioandroid.model.GitHubPullModel;
import gabrielcosta.com.br.desafioandroid.model.GitHubPullModelImpl;

public class PullRequestActivity extends AppCompatActivity implements GitHubDelegate<List<Pullrequest>>, PullListClickDelegate {

    public static final String EXTRA_REPOSITORY = "repository";
    public static final String EXTRA_CREATOR = "creator";

    private GitHubPullModel model;
    private String currentRepository;
    private String currentCreator;
    private PullListAdapter adapter;
    private RecyclerView recyclerView;
    private List<Pullrequest> pullRequestList = new ArrayList<>();
    private ProgressDialog progressDialog;
    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.currentCreator = getIntent().getStringExtra(EXTRA_CREATOR);
        this.currentRepository = getIntent().getStringExtra(EXTRA_REPOSITORY);

        this.getSupportActionBar().setTitle(this.currentRepository);

        this.progressDialog = this.buildProgressDialog();

        this.recyclerView = (RecyclerView) findViewById(R.id.pull_list);
        this.adapter = new PullListAdapter(this.pullRequestList, this.getBaseContext(), this);
        this.recyclerView.setAdapter(this.adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        this.recyclerView.setLayoutManager(linearLayoutManager);

        this.model = new GitHubPullModelImpl(this);
        this.loadList();

        this.recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                PullRequestActivity.this.loadList();
            }
        });
    }

    private void loadList() {
        PullRequestActivity.this.progressDialog.show();
        this.model.listPullRequest(currentCreator, currentRepository, this.page++);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSucess(List<Pullrequest> pullrequests) {

        this.adapter.incrementAdapter(pullrequests);
        this.progressDialog.dismiss();

    }

    @Override
    public void onFail(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Retornar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        NavUtils.navigateUpFromSameTask(PullRequestActivity.this);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

        this.progressDialog.dismiss();
    }

    @Override
    public void onItemClick(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    private ProgressDialog buildProgressDialog() {
        ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Carregando");
        progress.setMessage("Carregando lista de pull request");
        progress.setCancelable(false);
        return progress;
    }
}
