package gabrielcosta.com.br.desafioandroid.delegate;

/**
 * Created by gabrielcosta on 16/07/16.
 */
public interface ViewHolderClickDelegate {

    void onItemclick(String creator, String repository);

}
