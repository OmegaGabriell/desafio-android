package gabrielcosta.com.br.desafioandroid.model;

import android.util.Log;

import gabrielcosta.com.br.desafioandroid.delegate.GitHubDelegate;
import gabrielcosta.com.br.desafioandroid.entity.Item;
import gabrielcosta.com.br.desafioandroid.entity.Pullrequest;
import gabrielcosta.com.br.desafioandroid.entity.RepositoryResult;
import gabrielcosta.com.br.desafioandroid.entity.UserInfo;
import gabrielcosta.com.br.desafioandroid.service.GitHubService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gabriel.costa on 15/07/2016.
 */
public class GitHubModelImpl implements GitHubModel, Callback<RepositoryResult> {

    private GitHubDelegate delegate;
    private GitHubService service;

    public GitHubModelImpl(GitHubDelegate delegate) {
        this.delegate = delegate;
        this.service = new GitHubService();
    }


    @Override
    public void listRepositoriesByLanguage(String language, int page) {
        this.service.listRepositoriesByLanguage(language, page).enqueue(this);
    }

    @Override
    public void onResponse(Call<RepositoryResult> call, Response<RepositoryResult> response) {

        if (response.isSuccessful()) {
            for (Item item : response.body().getItems()) {

                final Item acessItem = item;

                this.service.getUserInfo(item.getOwner().getLogin()).enqueue(new Callback<UserInfo>() {
                    @Override
                    public void onResponse(Call<UserInfo> call, Response<UserInfo> userResponse) {
                        if (acessItem.getOwner() != null && userResponse.body() != null) {
                            acessItem.getOwner().setFullUserName(userResponse.body().getName());
                        } else {
                            acessItem.getOwner().setFullUserName(acessItem.getOwner().getLogin());
                        }
                    }

                    @Override
                    public void onFailure(Call<UserInfo> call, Throwable t) {
                        Log.e("ERRO_LOAD_ITEM", "Erro ao carregar item");
                    }
                });

            }
            this.delegate.onSucess(response.body().getItems());
        } else {
            switch (response.code()) {
                case 403:
                    this.delegate.onFail("Número de chamadas sem autenticar excedidas para o ip atual");
                    break;
                default:
                    this.delegate.onFail("Erro ao carregar a lista de pull request");
            }
        }

    }

    @Override
    public void onFailure(Call<RepositoryResult> call, Throwable t) {
        this.delegate.onFail("Erro ao carregar lista");
    }
}
