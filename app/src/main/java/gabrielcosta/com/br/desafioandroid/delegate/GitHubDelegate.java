package gabrielcosta.com.br.desafioandroid.delegate;

import java.util.List;

/**
 * Created by gabriel.costa on 15/07/2016.
 */
public interface GitHubDelegate<T> {

    void onSucess(T t);

    void onFail(String message);

}
