package gabrielcosta.com.br.desafioandroid.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import gabrielcosta.com.br.desafioandroid.R;
import gabrielcosta.com.br.desafioandroid.delegate.ViewHolderClickDelegate;

/**
 * Created by gabrielcosta on 14/07/16.
 */
public class RepositoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView repositoryName;

    public TextView repositoryDesc;

    public TextView repositoryForks;

    public TextView repositoryStarts;

    public ImageView repositoryImage;

    public TextView repositoryUsername;

    public TextView userLastName;

    private ViewHolderClickDelegate delegate;

    public RepositoryHolder(View itemView, ViewHolderClickDelegate delegate) {
        super(itemView);
        this.repositoryName = (TextView) itemView.findViewById(R.id.repository_name);
        this.repositoryDesc = (TextView) itemView.findViewById(R.id.repository_desc);
        this.repositoryForks = (TextView) itemView.findViewById(R.id.repository_fork);
        this.repositoryStarts = (TextView) itemView.findViewById(R.id.repository_star);
        this.repositoryImage = (ImageView) itemView.findViewById(R.id.repository_image);
        this.repositoryUsername = (TextView) itemView.findViewById(R.id.repository_username);
        this.userLastName = (TextView) itemView.findViewById(R.id.repository_user_last_name);
        itemView.setOnClickListener(this);
        this.delegate = delegate;
    }

    @Override
    public void onClick(View view) {
        this.delegate.onItemclick(this.repositoryUsername.getText().toString(), this.repositoryName.getText().toString());
    }

}
