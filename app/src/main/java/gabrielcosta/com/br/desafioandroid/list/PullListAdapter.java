package gabrielcosta.com.br.desafioandroid.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import gabrielcosta.com.br.desafioandroid.R;
import gabrielcosta.com.br.desafioandroid.delegate.PullListClickDelegate;
import gabrielcosta.com.br.desafioandroid.entity.Pullrequest;

/**
 * Created by gabrielcosta on 16/07/16.
 */
public class PullListAdapter extends RecyclerView.Adapter<PullListHolder> {

    private List<Pullrequest> pullList;
    private final Context context;
    private final PullListClickDelegate delegate;

    public PullListAdapter(List<Pullrequest> list, Context context, PullListClickDelegate delegate) {
        this.pullList = list;
        this.context = context;
        this.delegate = delegate;
    }

    @Override
    public PullListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pull_list_item, parent, false);

        return new PullListHolder(view, this.delegate);
    }

    @Override
    public void onBindViewHolder(PullListHolder holder, int position) {
        Pullrequest pullrequest = this.pullList.get(position);

        holder.pullRequestName.setText(pullrequest.getTitle());
        holder.pullRequestDesc.setText(pullrequest.getBody());
        holder.userName.setText(pullrequest.getUser().getLogin());

        if (pullrequest.getUser().getFullUserName() != null) {
            holder.userLastName.setText(pullrequest.getUser().getFullUserName());
        }

        Picasso.with(this.context)
                .load(pullrequest.getUser().getAvatarUrl())
                .placeholder(R.drawable.ic_action_account_circle)
                .into(holder.userImage);

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date pullDate = simpleDateFormat.parse(pullrequest.getCreatedAt());
            holder.pullDate.setText("Criado em: " + SimpleDateFormat.getInstance().format(pullDate));
        } catch (ParseException e) {
            e.printStackTrace();
            holder.pullDate.setText("");
        }

        holder.setPullUrl(pullrequest.getHtmlUrl());
    }

    public void incrementAdapter(List<Pullrequest> pullList) {
        this.pullList.addAll(pullList);
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return pullList.size();
    }
}
