package gabrielcosta.com.br.desafioandroid.model;

import java.util.ArrayList;
import java.util.List;

import gabrielcosta.com.br.desafioandroid.delegate.GitHubDelegate;
import gabrielcosta.com.br.desafioandroid.entity.Pullrequest;
import gabrielcosta.com.br.desafioandroid.entity.UserInfo;
import gabrielcosta.com.br.desafioandroid.service.GitHubService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gabrielcosta on 16/07/16.
 */
public class GitHubPullModelImpl implements GitHubPullModel, Callback<List<Pullrequest>> {

    private GitHubDelegate<List<Pullrequest>> delegate;
    private GitHubService service;

    public GitHubPullModelImpl(GitHubDelegate delegate) {
        this.delegate = delegate;
        this.service = new GitHubService();
    }

    @Override
    public void listPullRequest(String creator, String repository, int page) {
        service.listRepositoryPullRequest(creator, repository, page).enqueue(this);

    }

    @Override
    public void onResponse(Call<List<Pullrequest>> call, Response<List<Pullrequest>> response) {

        if (response.isSuccessful()) {
            for (Pullrequest item : response.body()) {

                final Pullrequest acessItem = item;

                this.service.getUserInfo(item.getUser().getLogin()).enqueue(new Callback<UserInfo>() {
                    @Override
                    public void onResponse(Call<UserInfo> call, Response<UserInfo> userResponse) {
                        if (acessItem.getUser() != null && userResponse.body() != null) {
                            acessItem.getUser().setFullUserName(userResponse.body().getName());
                        }
                    }

                    @Override
                    public void onFailure(Call<UserInfo> call, Throwable t) {

                    }
                });

            }
            this.delegate.onSucess(response.body());
        } else {
            switch (response.code()) {
                case 403:
                    this.delegate.onFail("Número de chamadas sem autenticar excedidas para o ip atual");
                    break;
                default:
                    this.delegate.onFail("Erro ao carregar a lista de pull request");
            }
        }


    }

    @Override
    public void onFailure(Call<List<Pullrequest>> call, Throwable t) {

    }
}
