package gabrielcosta.com.br.desafioandroid.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import gabrielcosta.com.br.desafioandroid.R;
import gabrielcosta.com.br.desafioandroid.delegate.PullListClickDelegate;

/**
 * Created by gabrielcosta on 16/07/16.
 */
public class PullListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView pullRequestName;
    public TextView pullRequestDesc;
    public ImageView userImage;
    public TextView userName;
    public TextView userLastName;
    public TextView pullDate;
    private String pullUrl;
    private PullListClickDelegate delegate;

    public PullListHolder(View itemView, PullListClickDelegate delegate) {
        super(itemView);

        this.pullRequestName = (TextView) itemView.findViewById(R.id.pull_request_name);
        this.pullRequestDesc = (TextView) itemView.findViewById(R.id.pull_request_desc);
        this.userImage = (ImageView) itemView.findViewById(R.id.user_image);
        this.userName = (TextView) itemView.findViewById(R.id.user_username);
        this.userLastName = (TextView) itemView.findViewById(R.id.user_user_last_name);
        this.pullDate = (TextView) itemView.findViewById(R.id.pull_date);

        itemView.setOnClickListener(this);
        this.delegate = delegate;
    }

    public void setPullUrl(String pullUrl) {
        this.pullUrl = pullUrl;
    }

    @Override
    public void onClick(View view) {
        delegate.onItemClick(pullUrl);
    }
}
