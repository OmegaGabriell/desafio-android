package gabrielcosta.com.br.desafioandroid.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gabrielcosta.com.br.desafioandroid.entity.Pullrequest;
import gabrielcosta.com.br.desafioandroid.entity.RepositoryResult;
import gabrielcosta.com.br.desafioandroid.entity.UserInfo;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by gabriel.costa on 15/07/2016.
 */
public class GitHubService {

    private static final String GITHUB_DOMAIN = "https://api.github.com";

    private final Retrofit retrofit;
    private final GitHubRetrofitService service;

    public GitHubService() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(GITHUB_DOMAIN)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.service = retrofit.create(GitHubRetrofitService.class);
    }

    public Call<RepositoryResult> listRepositoriesByLanguage(String language, int page) {

        Map<String, String> queryMap = new HashMap<>();
        queryMap.put("page", String.valueOf(page));
        queryMap.put("sort", "stars");
        queryMap.put("q", language);

        return service.listRepositories(queryMap);
    }

    public Call<List<Pullrequest>> listRepositoryPullRequest(String creator, String repository, int page) {

        return service.listPullRequest(creator, repository, page);
    }

    public Call<UserInfo> getUserInfo(String login) {

        return service.getUserInfo(login);
    }

}
